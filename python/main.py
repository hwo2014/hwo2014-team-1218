import json
import socket
import sys
import math


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.throttleValue = 1.0
        self.targetSpeed = 10
        self.lap = 0
        # Game data
        self.lanes = None
        self.pieces = None
        self.numPieces = None
        self.numLanes = None
        # Game parameters - calculated once
        self.power = None
        self.drag = None
        self.speedCalibrated = False
        self.maxSpeed = None
        # Current state values
        self.curSpeed = 0  # distance / tick - assumes each tick is equal physics-wise
        self.inCurve = False
        self.safeSpeeds = {}
        self.dangerSpeeds = {}
        # Scratch values
        self.curTick = -1
        self.lastSentTick = -2
        self.lastPosition = None
        self.lastAngle = 0
        self.dangerSpeed = 100000
        self.safeSpeed = 0

    def msg(self, msg_type, data):
#        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.curTick}))
        if self.curTick<0 or (self.curTick>=0 and self.curTick != self.lastSentTick):
            self.send(json.dumps({"msgType": msg_type, "data": data}))
            self.lastSentTick = self.curTick

    def send(self, msg):
#        print msg
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": "usa", "carCount":1})
        #return self.msg("join", {"name": self.name, "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        self.lanes = data['race']['track']['lanes']
        self.pieces = data['race']['track']['pieces']
        self.numLanes = len(self.lanes)
        self.numPieces = len(self.pieces)
        for i  in range(0, self.numPieces):
            self.pieces[i]['pieceIndex'] = i

        print("Game init")
        print(self.pieces)

    def on_your_car(self, data):
        self.mycolor = data['color'];


    def on_game_start(self, data):
        print("Race started")
        self.throttle(1.0)

    def piece_dist(self, thisPiece, startLaneIndex, endLaneIndex):
        if 'length' in thisPiece:
            return thisPiece['length']
            # TODO: lane switch adds something?
        else:
            # TODO: add diff for lane switch
            angle = thisPiece['angle']
            if angle == 0:
                return 0
            angleValue = abs(angle)
            angleSign = angle / angleValue
            radius = thisPiece['radius']
            radius  -= angleSign * (
                self.lanes[startLaneIndex]['distanceFromCenter'] +
                self.lanes[endLaneIndex]['distanceFromCenter']) /2
            # Just assume switching in the middle for now, unless we figure out better.
            return radius * math.pi * angleValue / 180

    def calc_throttle_value(self, maximizer = True):
        # Maximizer starts slowing down as little as possible
        # print "ts: {}  cs: {}".format(self.targetSpeed, self.curSpeed)
        self.throttleValue = ((self.targetSpeed - self.curSpeed
            - self.curSpeed * self.drag) / self.power)
        if self.throttleValue > 1: self.throttleValue = 1
        elif self.throttleValue < 0: 
            if not maximizer:
                self.throttleValue = 0
            else:  # efficient slowing down
                curSpeed = self.targetSpeed
                lastSpeed = None
                while curSpeed<self.curSpeed:
                    # Algebra for how to derive start speed from a given end-speed and drag
                    # a = endspeed, b = startspeed, c = drag ratio
                    #   a = b + b*c  (drag is negative in our calculations)
                    #   a = b * (1+c)
                    #   b = a / (1+c)  <---
                    lastSpeed = curSpeed
                    curSpeed = curSpeed  / (1+ self.drag)

                # then, calculate what the speed for our very next tick should be
                nextTickSpeed = lastSpeed
                self.throttleValue = ((nextTickSpeed - self.curSpeed
                    - self.curSpeed * self.drag) / self.power)
                if self.throttleValue<0: self.throttleValue = 0
#                print "nextTickSpeed: {}, throttleValue: {}".format(nextTickSpeed, self.throttleValue)

#        print "Throttle value: {}".format(self.throttleValue)

    def calc_distance_to_reach_speed(self, startSpeed, endSpeed):
        if endSpeed==startSpeed:
            return 0
        elif endSpeed < startSpeed:
            numTicks = 0
            sumDist = 0
            while startSpeed>endSpeed:
                startSpeed = max(startSpeed + startSpeed * self.drag, endSpeed)
                sumDist += startSpeed
            return sumDist
        # calculating for increasing speed not yet done - prob won't be needed

    def set_max_speed_if_necessary(self, thisPiece, endSpeed, lane):
        curMaxSpeed = thisPiece['maxSpeed'][lane]
        thisPieceDistance = self.piece_dist(thisPiece, lane, lane)
        if endSpeed<curMaxSpeed:
            sumDist=0

            while endSpeed<curMaxSpeed:
                endSpeed = endSpeed / (1 + self.drag)
                sumDist+=endSpeed  # TOCHECK: should this be before endspeed?
                if sumDist >= thisPieceDistance:  # okay to be a little approximate?
                    self.set_max_speed(thisPiece, endSpeed, lane)
                    break

    def set_max_speed(self, thisPiece, maxSpeed, lane):
        # TODO: lane-specific?  I guess so!
        # Set and propogate backwards max speed for a piece, so that we are at most this speed when we begin this piece.
        # TOMAYBE: break out if speed parameters not yet initialized
        if maxSpeed < thisPiece['maxSpeed'][lane]:
            thisPiece['maxSpeed'][lane] = maxSpeed
            #check previous piece
            prevPiece = self.pieces[(thisPiece['pieceIndex']-1+self.numPieces)%self.numPieces]
            self.set_max_speed_if_necessary(prevPiece, maxSpeed, lane)

    def on_car_positions(self, data):
        # TODO: split into function that runs once for each car's position info (?)
        # TODO: a lot of data gathering sections run on all cars, some sections only  run on our car
        myCarData = data[0] # To be specific for my own car - find my data
        thisPosition = myCarData['piecePosition']  
        lastPosition = self.lastPosition
        self.lastPosition = thisPosition
        startLaneIndex = thisPosition['lane']['startLaneIndex']
        endLaneIndex = thisPosition['lane']['endLaneIndex']
        # TODO: do some of these once only when entering new piece?

        if lastPosition:
            # Calculate distance travelled since last tick
            thisPieceIndex = thisPosition['pieceIndex']
            lastPieceIndex = lastPosition['pieceIndex']
            distTravelled = thisPosition['inPieceDistance'] - lastPosition['inPieceDistance']

            if thisPieceIndex!=lastPieceIndex:
                print self.pieces[thisPieceIndex]

            while (thisPieceIndex != lastPieceIndex):  # add for if we went over some pieces completely
                distTravelled += self.piece_dist(self.pieces[lastPieceIndex], 
                    startLaneIndex, endLaneIndex)
                lastPieceIndex = (lastPieceIndex + 1) % self.numPieces
            self.curSpeed = distTravelled
            print("{}, {}, {}, {}, {}, {}".format(
                self.lap, 
                thisPieceIndex, 
                thisPosition['inPieceDistance'], 
                distTravelled,
                myCarData['angle'],
                self.throttleValue
            ))

            thisPiece = self.pieces[thisPieceIndex]
            if not self.speedCalibrated:
            # Power and drag detection
                if not self.power:
                    self.power = distTravelled
                    print "Power: {}".format(self.power)
                else:
                    self.drag = (distTravelled / self.power) - 2
                    # from diff in distances travelled / first tick velocity
                    self.maxSpeed = - self.power / self.drag
                    for thisPiece in self.pieces:
                        thisPiece['maxSpeed'] = [0] * self.numLanes
                        for i in range(0, self.numLanes):
                            thisPiece['maxSpeed'][i] = self.maxSpeed
                        
                    self.speedCalibrated = True
                    print ("Power: {}  Drag: {}".format(self.power, self.drag))
            else:
                if 'radius' in thisPiece:
                    thisRadius = thisPiece['radius']
                    thisAngle = thisPiece['angle']
                    turnDir = thisAngle/abs(thisAngle)  #+1 for right, -1 for left
                    thisRadius -= turnDir * (
                        self.lanes[startLaneIndex]['distanceFromCenter'] +
                        self.lanes[endLaneIndex]['distanceFromCenter']) /2

                    if not self.inCurve:
                        self.inCurve = True
                        if thisRadius in self.safeSpeeds:
                            self.safeSpeed = self.safeSpeeds[thisRadius]
                        else:
                            self.safeSpeed = 0
                        if thisRadius in self.dangerSpeeds:
                            self.dangerSpeed = self.dangerSpeeds[thisRadius]
                        else:
                            self.dangerSpeed = 10
                        self.dangerSpeed = min(self.dangerSpeed, thisPiece['maxSpeed'][startLaneIndex])
                    else:
                        # only do these in else so we don't count first tick in curve, where the physics haven't applied to the car yet
                        angle = myCarData['angle']
                        angleDif = turnDir*(angle-self.lastAngle)
                        if angleDif >1:
                            self.dangerSpeed = min(self.dangerSpeed, self.curSpeed)
#                            print "dangerSpeed: {}".format(self.dangerSpeed)
                            self.dangerSpeeds[thisRadius] = self.dangerSpeed
                            self.set_max_speed(thisPiece, self.dangerSpeed, startLaneIndex)
#                            if abs(angle) > 10:
#                                self.targetSpeed = self.safeSpeed
                        elif angleDif<=0:
                            self.safeSpeed = max(self.safeSpeed, self.curSpeed)
#                            self.targetSpeed = ( self.dangerSpeed + self.safeSpeed ) /2
#                            print "safeSpeed: {}".format(self.safeSpeed)
                            self.safeSpeeds[thisRadius] = self.safeSpeed
                        self.targetSpeed = ( self.dangerSpeed + self.safeSpeed ) /2
                else:
                    #self.targetSpeed = 6 + self.lap * 2
                    self.targetSpeed = min(self.maxSpeed, thisPiece['maxSpeed'][startLaneIndex])
                    self.inCurve = False
                    # TODO: update safeSpeeds and dangerSpeeds here instead?

                self.calc_throttle_value(not self.inCurve)  # Optimal throttle value for reaching desired speed
                # use Maximizer only when on straights, to not mess with the speed juggling in curves.
#                self.targetSpeed = 8 + self.lap
#                self.calc_throttle_value()
                self.throttle(self.throttleValue)
            self.lastAngle = myCarData['angle']

        else: # lastPosition false
            print("lap, thisPieceIndex, inPieceDistance, Speed, angle")

        # failsafe in case nothing got triggered (will we ever have some other place we should do this instead?)
        #print self.throttleValue
        self.throttle(self.throttleValue)



    def on_crash(self, data):
        print("Someone crashed")

    def on_lap_finished(self, data):
        # TODO: check for our car / track for other cars?
        print data
        self.lap = data['lapTime']['lap'] + 1

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'lapFinished': self.on_lap_finished,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.curTick = msg['gameTick']
#               print(msg['gameTick'])
#            print msg_type
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                print data
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
